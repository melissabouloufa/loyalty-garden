DROP DATABASE IF EXISTS loyalty_garden; 

CREATE DATABASE loyalty_garden;

\c loyalty_garden

DROP TABLE IF EXISTS inscrits; 
DROP TABLE IF EXISTS produits;
DROP TABLE IF EXISTS panier;

CREATE USER loyalty_garden WITH PASSWORD 'loyalty_garden';

CREATE TYPE categorie AS ENUM ('plante', 'bouquet', 'pot', 'anniversaire');
CREATE TYPE couleur AS ENUM ('vert', 'marron', 'terre-cuite', 'gris', 'noir', 'bordeaux');

CREATE TABLE inscrits (
    id SERIAL PRIMARY KEY,
    mdp VARCHAR(255),
    nom VARCHAR(50),
    prenom VARCHAR(50),
    email VARCHAR(255),
    dateNaiss DATE,
    points INTEGER
);

CREATE TABLE produits (
    id SERIAL PRIMARY KEY,
    nom VARCHAR(255),
    prix INTEGER, 
    categorie categorie,
    photo TEXT,
    achats INTEGER
);

CREATE TABLE panier (
    id_client INTEGER REFERENCES inscrits(id),
    id_produit INTEGER REFERENCES produits(id), 
    quantite INTEGER,
    couleur couleur
);

GRANT ALL PRIVILEGES ON TABLE produits TO loyalty_garden;
GRANT USAGE, SELECT ON SEQUENCE produits_id_seq TO loyalty_garden;

GRANT ALL PRIVILEGES ON TABLE inscrits TO loyalty_garden;
GRANT USAGE, SELECT ON SEQUENCE inscrits_id_seq TO loyalty_garden;

GRANT ALL PRIVILEGES ON TABLE panier TO loyalty_garden;
GRANT USAGE, SELECT ON SEQUENCE inscrits_id_seq TO loyalty_garden;

INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Rosier rose', 1500, 'plante', '/shopping/produits/rosier-rose.jpeg',0);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Rosier rouge', 1500, 'plante', '/shopping/produits/rosier-rouge.jpeg',0);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Pot rond', 200, 'pot', '/shopping/produits/pot-rond.jpeg',0);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Pot carré', 200, 'pot', '/shopping/produits/pot-carre.jpeg',0);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Bouquet de tulipes', 1900, 'bouquet', '/shopping/produits/bouquet-tulipe.jpg',0);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Bouquet de jonquilles', 1800, 'bouquet', '/shopping/produits/bouquet-jonquille.jpeg',0);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Bouquet de roses', 2000, 'bouquet', '/shopping/produits/bouquet-rose.jpeg',0);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Petit olivier', 2500, 'plante', '/shopping/produits/petit-olivier.jpeg',1);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Fraisier', 1300, 'plante', '/shopping/produits/fraisier.jpeg',0);

INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Orchidée', 700, 'anniversaire', '/shopping/produits/fleur-anniversaire.jpg',0);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Cours de jardinage', 500, 'anniversaire', '/shopping/produits/cours.png',0);
INSERT INTO produits (nom,prix,categorie,photo,achats) VALUES ('Pot Spécial', 0, 'anniversaire', '/shopping/produits/pot-anniversaire.jpg',0);

INSERT INTO inscrits (mdp,nom,prenom,email,datenaiss,points) VALUES ('$2b$10$7I8QUqgprFRLoC8ZwDvAMe5nzrhJBCgR7WXYSU4fLoxrj/UyQG.eq','Bouloufa','Melissa','melissa@gmail.com','2003-08-14',0);
INSERT INTO inscrits (mdp,nom,prenom,email,datenaiss,points) VALUES ('$2b$10$sqknl/9uW3vZhPhzbpttM.xljdCJNvHeUGDqMnTz8RhGbvabSdEnO','Saidoune','Elyas','elyas@gmail.com','2003-12-17',0);
INSERT INTO inscrits (mdp,nom,prenom,email,datenaiss,points) VALUES ('$2b$10$ZmjVN9EPoCrm4WaYx.JJKOz8m5YArNldVl7XGizmOh0xXeAF8CEsS','Salar','Mona','mona@gmail.com','2005-04-02',0);
INSERT INTO inscrits (mdp,nom,prenom,email,datenaiss,points) VALUES ('$2b$10$LbJflOOjowGj/0r6NHgDheB8YzviVjapKLMsOmStUXwFoVNXsciTO','Tirou','Jean','tirou@gmail.com','1999-05-17',0);
INSERT INTO inscrits (mdp,nom,prenom,email,datenaiss,points) VALUES ('$2b$10$nLuTr5FAILHHXqnU89NrAOJGBmNPZ2Pw9ht9tM0NEJM1SJurN/52O','Maafa','Isa','isa@gmail.com','2002-12-08',0);
INSERT INTO inscrits (mdp,nom,prenom,email,datenaiss,points) VALUES ('$2b$10$LGr4huT0Bo6SdckE4cG0mOdtPAP36IG5WIA4SegaxKGTJeiCbAOu6','Saidoune','Lyes','lyes@gmail.com','1990-06-30',0);
INSERT INTO inscrits (mdp,nom,prenom,email,datenaiss,points) VALUES ('$2b$10$KEjPLnLTG2aKMnkBvQEgIusZw3YFTU6RU7adHmGwcQ56YXDQdeFKS','Belgur','Matew','matew@gmail.com','1965-10-21',0);
INSERT INTO inscrits (mdp,nom,prenom,email,datenaiss,points) VALUES ('$2b$10$GOyhgLWiUi6fVvup1/mcau71SGHeklm.9V7xtP.P3dGa34ceC5BGm','Test','Test','test@gmail.com','2000-01-01',0);