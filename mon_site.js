const v = require('./Visiteur');
const mot_de_passe = "gerante"; //mdp de la gérante
let gerante = false;
let connecte = false;
let client = -1;

async function run() {

    await v.connect();

    const express = require("express");
    /* inclure les en-têtes CORS nécessaires dans 
    ses répnonses HTTP pour autoriser les requêtes cross-origin*/
    const cors = require('cors');
    const path = require('path');
    const server = new express();
    server.use(express.json());
    server.use(express.urlencoded({extended:true}));
    server.set('views', 'public');
    server.use(express.static('public'));
    server.use(cors({
        origin: 'https://localhost:8080'
    }));
    const PORT = 8080;

    const multer  = require('multer'); // pour les fichiers
    const fs = require('fs');
    const upload = multer({dest : "public/shopping/produits/" });

    // recherche d'un membre
    server.get("/loyalty-garden/gerante/search", async function(req, res) {
        if(gerante) {
            let prenom = req.query.prenom;
            let nom = req.query.nom;
            let research;
            if (prenom===undefined || nom===undefined) {
                research = await v.rechercheMembre("");
            }
            else {
                research = await v.rechercheMembre(prenom, nom);
            }
            res.json(research);
        }
    });

    // page gérante
    server.get("/gerante", async function(req, res){
        if (!gerante && !connecte) {
            res.sendFile("connexion_gerante.html", {root: 'public/gerante'});
        }
        else if(gerante) {
            res.sendFile("gerante.html", {root: 'public/gerante'});
        }
        else {
            res.redirect("/shopping");
        }
    });

    server.post("/loyalty-garden/gerante", async function(req, res) {
        let bool = req.body.mdp === mot_de_passe;
        if (bool) {
            gerante = true;
            res.json(true);
        }
        else {
            res.json(false);
        }
    });

    // accueil du site
    server.get("/", async function(req, res){
        if(!connecte && !gerante) 
            res.sendFile("accueil.html", {root: 'public/accueil'});
        else if (connecte)
            res.redirect("/shopping");
        else 
            res.redirect("/gerante");
    });

    server.post("/connexion", async function(req, res){
        if(!connecte && !gerante) {
            let email = req.body.email;
            let mdp = req.body.mdp;
            let tab = await v.connexion(email, mdp);
            if(tab[0]) {
                connecte = true;
                client = tab[1];
                res.json(true);
            }
            else {
                res.json(false);
            }
        }
    });

    // vérifie si c'est l'anniversaire du client
    server.get("/anniversaire", async function(req, res){
        let anniversaire = await v.anniversaire(client);
        res.json(anniversaire);
    });

    // Création d'un compte par la gérante
    server.get("/inscription", async function(req, res){
        if(gerante){
            res.sendFile("ajout_membre.html", {root: 'public/gerante'});
        }
        else {
            res.redirect("/shopping");
        }
    });

    server.post("/loyalty-garden/gerante/inscription", async function(req, res){
        if(gerante) {
            let nom = req.body.nom;
            let prenom = req.body.prenom;
            let mdp = req.body.mdp;
            let email = req.body.email;
            let naissance = req.body.naissance;
            if(await v.inscription(nom, prenom, mdp, email, naissance)){
                res.json(true);
            }
            else {
                res.json(false);
            }
        }
    });

    // Création et ajout d'un cadeau au catalogue
    server.get("/gerante/ajout_article", async function(req, res){
        if(gerante){
            res.sendFile("ajout_article.html", {root: 'public/gerante'});
        }
        else{
            res.redirect("/shopping")
        }
    });

    server.post("/loyalty-garden/gerante/ajout_article", upload.single('file'), async function(req, res){
        if(gerante) {
            let nom = req.body.nom;
            let prix = req.body.prix;
            let categorie = req.body.categorie;
            let photo = "image"+(await v.dernier_id())+".jpeg";
            photo = "/shopping/produits/"+photo;
            let new_path = "public/"+photo;
            let tab = [];

            fs.rename(req.file.path, new_path, (err) => {
                if(err){
                    console.error(err);
                    return;
                } 
            });

            tab[0] = await v.ajoutProduit(categorie, nom, prix, photo);
            tab[1] = photo;

            res.json(tab);
        }
    });

    server.post("/loyalty-garden/suppProduit", async function(req, res) {
        if(gerante) {
            let supprime = await v.suppProduit(req.body.id);
            if(supprime[1]) {
                let cheminFichier = 'public'+supprime[0];
                if (fs.existsSync(cheminFichier)) {
                    fs.unlinkSync(cheminFichier);
                }
            }
            res.json(supprime[1]);
        }
    });

    server.post("/loyalty-garden/suppMembre", async function(req, res) {
        if(gerante) {
            res.json(await v.suppMembre(req.body.id));
        }
    });

    server.post("/loyalty-garden/modifPoints", async function(req, res){
        if (gerante) {
            res.json(await v.modifPoints(+req.body.pts, req.body.id));
        }
    });

    server.post("/loyalty-garden/gerante/search2", async function(req, res){
        if(gerante) {
            let nom = req.body.nom;
            let research = [];
            if (nom===undefined) {
                research = await v.rechercheProduit("");
            }
            else {
                research = await v.rechercheProduit(nom);
            }
            res.json(research);
        }
    });

    server.get("/connexion", async function(req, res){
        if(!connecte && !gerante){
            res.sendFile("connexion.html", {root: 'public/connexion'});
        }
        else if (connecte) {
            res.redirect("/shopping");
        }
        else {
            res.redirect("/gerante");
        }
    });

    server.get("/deconnexion", async function(req, res) {
        connecte = false;
        gerante = false;
        res.json(true);
    });

    server.get("/shopping", async function(req, res){
        if(connecte) {
            let pts = await v.pointsClient(client);
            let data = {
                pots : await v.pots(),
                plantes : await v.plantes(),
                bouquets : await v.bouquets(),
                populaires : await v.populaires(),
                recommandations : await v.recommandation(client),
                restant : pts-(await v.prixPanier(client)),
                points : pts,
                anniversaire : await v.produitAnniversaire(client)
            }
            res.render('shopping/shopping.ejs', data);
        }
        else {
            res.redirect("/");
        }
    });

    server.post("/loyalty-garden/shopping",async function(req,res){
        if(connecte){
            let id = req.body.id;
            if(req.body.categorie=='plante'){
                let pot = await v.nomToID("Pot "+ req.body.pot);
                let couleur = req.body.couleur;
                await v.addPanier(client, id);
                await v.addPotPanier(client, pot, couleur);
            }
            else if(req.body.categorie=='pot'){
                let couleur = req.body.couleur;
                await v.addPotPanier(client, id, couleur);
            }
            else{
                await v.addPanier(client, id);
            }
            res.status(200).end();
        }
        else {
            res.redirect("/");
        }
    });

    server.get("/panier", async function(req, res){
        if(connecte) {
            let data = {
                panier : await v.panier(client),
                total : await v.prixPanier(client),
                points : await v.pointsClient(client)
            }
            res.render('panier/panier.ejs', data);
        }
        else {
            res.redirect("/loyalty-garden");
        }
    });

    server.post("/loyalty-garden/panier/supprimer", async function(req, res){
        if(connecte) {
            let id = req.body.id;
            let categorie = req.body.categorie;
            if(categorie=='pot'){
                let couleur = req.body.couleur;
                await v.supprimerArticlePanier(client, id, couleur);
            }
            else{
                await v.supprimerArticlePanier(client, id, null);
            }
            res.json(true);
        }
        else {
            res.redirect("/");
        }
    });

    server.post("/loyalty-garden/panier", async function(req, res){
        if(connecte) {
            res.json((await v.validerPanier(client)));
        }
        else {
            res.redirect("/loyalty-garden");
        }
    });

    server.listen(PORT, () => {
        console.log("Serveur demarré");
    });

}

run();