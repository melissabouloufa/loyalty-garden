const pg = require('pg');
const bcrypt = require('bcrypt'); // hachage des mots de passe

function Visiteur () { // client
    const pool = new pg.Pool({
        user: 'loyalty_garden',
        host: 'localhost',
        database: 'loyalty_garden',
        password: 'loyalty_garden',
        port: 5432
    });

    let v;

    this.connect = async function() {
        v = await pool.connect();
    };

    // Hacher un mot de passe
    this.hachage = async function hashPassword(password) {
        const saltRounds = 10;
        const hash = await bcrypt.hash(password, saltRounds);
        return hash;
    }
    
    // Vérifier un mot de passe
    this.verifier = async function verifyPassword(password, hash) {
        return await bcrypt.compare(password, hash);
    }

    this.inscription = async function(nom, prenom, mdp, email, naissance) {
        // vérifier si le client a déjà un compte
        const res = await v.query('SELECT email \
                                   FROM inscrits \
                                   WHERE email LIKE $1', [email]);
        if (res.rows.length!=0) {
            // erreur email existe déjà
            return false;
        }
        mdp = await this.hachage(mdp);
        await v.query('INSERT INTO inscrits \
                        (nom, prenom, mdp, email, dateNaiss, points) \
                        VALUES ($1, $2, $3, $4, $5, $6)', 
                        [nom, prenom, mdp, email, naissance, 0]);
        return true;
    }

    // Vérifier si c'est l'anniversaire du client id
    this.anniversaire = async function(id) {
        let res_requete = await v.query('SELECT prenom, datenaiss\
                                       FROM inscrits\
                                       WHERE id=$1', [id]);
        let naissance = new Date(res_requete.rows[0].datenaiss);
        let aujourdhui = new Date();

        let mois_naissance = naissance.getMonth()+1;
        let mois_aujourdhui = aujourdhui.getMonth()+1;
        let jour_naissance = naissance.getDate();
        let jour_aujourdhui  = aujourdhui.getDate();
        
        let tab = [];
        // je retourne true/false + le nom du client 
        tab[0] = mois_naissance===mois_aujourdhui
                && jour_naissance===jour_aujourdhui;
        tab[1] = res_requete.rows[0].prenom;
            
        return(tab);
    }

    // Connexion du client
    this.connexion = async function(email, mdp) {
        const tab = [];
        const email2 = email.toLowerCase();
        const res = await v.query('SELECT id, mdp \
                                   FROM inscrits \
                                   WHERE email LIKE $1', [email2]);
        if (res.rows.length==0) {
            return false;
        }
        tab[0] = await this.verifier(mdp,res.rows[0].mdp);
        tab[1] = res.rows[0].id;
        return tab;
    }

    /* Rechercher un membre (gerante)
        par nom ou par prénom, dans n'importe quel ordre */
    this.rechercheMembre = async function(prenom, nom) {
        let acc = [];
        let requete = '';
        let args = [];
        prenom = prenom.trim();
        nom = nom.trim();
        if(nom.length > 0 && prenom.length > 0) {
            const nom2 = nom[0].toUpperCase() + nom.substring(1).toLowerCase();
            const prenom2 = prenom[0].toUpperCase() + prenom.substring(1).toLowerCase();
            requete = 'SELECT * FROM inscrits \
                       WHERE (nom LIKE $1 AND prenom LIKE $2) \
                       OR (nom LIKE $2 AND prenom LIKE $1) \
                       ORDER BY nom';
            args = [nom2+'%', prenom2+'%'];
        }
        else if(prenom.length > 0) {
            const prenom2 = prenom[0].toUpperCase() + prenom.substring(1).toLowerCase();
            requete = 'SELECT * FROM inscrits \
                       WHERE nom LIKE $1 \
                       OR prenom LIKE $1 ORDER BY nom';
            args = [prenom2+'%'];
        }
        else {
            requete = 'SELECT * FROM inscrits ORDER BY nom';
        }

        try {
            const res = await v.query(requete, args);
            for (let p of res.rows) {
                acc.push(p);
            }
            return acc;
        }
        catch (err) {
            console.log("Une erreur est survenue.", err);
            return null;
        }
    }

    /* Retourner l'id du dernier produit créé
        pour pouvoir renommer son image */
    this.dernier_id = async function() {
        const last_id = await v.query('SELECT MAX(id)+1 AS id_max\
                                       FROM produits');
        return last_id.rows[0].id_max;
    }

    // Ajout d'un produit (gerante)
    this.ajoutProduit = async function(categorie, nom, prix, image) {
        const res = await v.query('SELECT nom FROM produits \
                                   WHERE nom LIKE $1', [nom]);
        if (res.rows.length!=0) {
            // erreur produit existe déjà
            return false;
        }
        await v.query('INSERT INTO produits \
                       (nom, prix, categorie, photo, achats) \
                       VALUES ($1, $2, $3, $4, $5)', 
                       [nom, prix, categorie, image, 0]);
        return true;
    }

    // Ajouter/enlever des points à un membre (gerante)
    this.modifPoints = async function(pts, id) {
        const points_avant = await this.pointsClient(id);
        let new_points = points_avant+pts;
        if (new_points < 0) new_points = 0;
        try {
            await v.query('UPDATE inscrits SET points=$1 WHERE \
                           id=$2',[new_points, id]);
            return true;
        }
        catch (err) {
            console.log("Une erreur est survenue.", err);
            return false;
        }
    }

    // Recherche d'un produit (gerante)
    this.rechercheProduit = async function(nom) {
        let acc = [];
        nom = nom.trim(); 
        /* on ne prend pas en considération les espaces 
        en début et fin de la recherche */
        try {
            const res = await v.query('SELECT * FROM produits \
                                       WHERE nom LIKE $1', [nom+'%']);
            for (let p of res.rows) {
                acc.push(p);
            }
            return acc;
        }
        catch (err) {
            console.log("Une erreur est survenue.", err);
            return null;
        }
    }

    // Supprimer un cadeau (gerante)
    this.suppProduit = async function(id) {
        try {
            let image = await v.query('SELECT photo\
                                      FROM produits\
                                      WHERE id=$1',[id])
            await v.query('DELETE FROM produits WHERE id=$1', [id]);
            return [image.rows[0].photo,true];
        }
        catch (err) {
            console.log("Une erreur est survenue.", err);
            return false;
        }
    }

    // Supprimer un membre (gerante)
    this.suppMembre = async function(id) {
        try {
            await v.query('DELETE FROM inscrits WHERE id=$1', [id]);
            return true;
        }
        catch (err) {
            console.log("Une erreur est survenue.", err);
            return false;
        }
    }

    // Fonctions pour retourner les cadeaux d'une catégroie
    this.plantes = async function(){
        let res = await v.query('SELECT * from produits \
                                WHERE categorie = \'plante\'');
        return res.rows;
    }

    this.pots = async function(){
        let res = await v.query('SELECT * from produits \
                                WHERE categorie = \'pot\'');
        return res.rows;
    }

    this.bouquets = async function(){
        let res = await v.query('SELECT * from produits \
                                WHERE categorie = \'bouquet\'');
        return res.rows;
    }

    this.populaires = async function(){
        return (await v.query('SELECT * from produits \
                                where NOT categorie=\'pot\' \
                                and NOT categorie=\'anniversaire\' \
                                order by achats DESC LIMIT 5')).rows;
    }

    this.recommandation = async function(client){
        return (await v.query("SELECT * from produits \
                                where prix <= (SELECT points \
                                               FROM inscrits \
                                               WHERE id=$1) \
                                and not categorie='anniversaire'"
                                ,[client])).rows;
    }

    this.produitAnniversaire = async function(client){
        return (await v.query('SELECT * from produits\
                             where categorie=\'anniversaire\'')).rows;
    }

    this.panier = async function(client){
        let res = await v.query('SELECT * from produits \
                                join panier on id=id_produit \
                                WHERE id_client=$1',[client]);
        return res.rows;
    }

    // Prix du panier
    this.prixPanier = async function(client){
        let res = await v.query('SELECT sum(prix*quantite) as total \
                                from produits join panier \
                                on id=id_produit \
                                WHERE id_client=$1',[client]);
        return res.rows[0].total;
    }

    this.pointsClient = async function(client){
        let res = await v.query('SELECT points \
                                from inscrits where id=$1',[client]);
        return res.rows[0].points;
    }

    // Ajout d'un cadeau au panier
    this.addPanier = async function(client, produit){

        let id_pot_special = (await v.query('SELECT id\
                                            FROM produits\
                                            WHERE nom \
                                            LIKE \'Pot Spécial\'')
                                          ).rows[0].id;

        let produitDansPanier = await v.query('SELECT id_client, \
                                                id_produit \
                                               FROM panier \
                                               WHERE id_client = $1 \
                                               and id_produit = $2', 
                                               [client, +produit]);
        if(produitDansPanier.rows.length==0){ //requete vide
            await v.query('INSERT INTO panier \
                            (id_client, id_produit, quantite) \
                            VALUES ($1, $2, $3)', [client, +produit, 1]);
            return;
        }
        // On ne peut le mettre qu'une seule fois dans le panier
        if (+produit === +id_pot_special) {
            return;
        }
        await v.query('UPDATE panier \
                        SET quantite=quantite+1 \
                        WHERE id_client=$1 \
                        and id_produit=$2', [client, +produit]);
    }

    this.addPotPanier = async function(client, produit, couleur){
        let produitDansPanier = await v.query('SELECT id_client, \
                                                id_produit \
                                                FROM panier \
                                                WHERE id_client = $1 \
                                                and id_produit = $2 \
                                                and couleur = $3', 
                                                [client, produit, couleur]);
        if(produitDansPanier.rows.length==0){ //requete vide
            await v.query('INSERT INTO panier \
                            (id_client, id_produit, quantite, couleur) \
                            VALUES ($1, $2, $3, $4)', 
                            [client, +produit, 1, couleur]);
            return;
        }
        await v.query('UPDATE panier \
                       SET quantite = quantite +1 \
                       WHERE id_client = $1 \
                       and id_produit = $2 \
                       and couleur=$3', 
                       [client, +produit, couleur]);

    }

    // Suppression d'un article du panier (cliente)
    this.supprimerArticlePanier = async function(client, produit, couleur){
        if(couleur!=null){
            let quantite = (await v.query('SELECT quantite \
                                            FROM panier \
                                            WHERE id_client=$1 \
                                            AND id_produit=$2 \
                                            AND couleur=$3',
                                            [client, produit, couleur])
                                            ).rows[0].quantite;
            if(quantite>1){
                await v.query('UPDATE panier \
                               SET quantite = quantite-1 \
                               WHERE id_client=$1 \
                               AND id_produit=$2 \
                               AND couleur=$3',
                               [client,produit,couleur]);
                return
            }
            await v.query('DELETE FROM panier \
                            WHERE id_client=$1 \
                            AND id_produit=$2 \
                            AND couleur=$3',
                            [client,produit,couleur]);
            return;
        }
        let quantite = (await v.query('SELECT quantite \
                                       FROM panier \
                                       WHERE id_client=$1 \
                                       AND id_produit=$2',
                                       [client, produit])
                                       ).rows[0].quantite;
            if(quantite>1){
                await v.query('UPDATE panier \
                               SET quantite = quantite-1 \
                               WHERE id_client=$1\
                               AND id_produit=$2',
                               [client,produit]);
                return
            }
            await v.query('DELETE FROM panier\
                           WHERE id_client=$1\
                           AND id_produit=$2',
                           [client,produit]);
            return;
    }

    this.validerPanier = async function(client){
        let prix = await this.prixPanier(client);
        if(prix > (await this.pointsClient(client))) return false;
        await v.query('DELETE FROM panier \
                       WHERE id_client=$1 ',[client]);
        await v.query('UPDATE inscrits \
                       SET points = points-$1\
                       WHERE id =$2',[prix, client])
        return true;
    }

    // Récupérer l'id d'un produit 
    this.nomToID = async function(nom){
        let id = await v.query('SELECT id\
                                from produits\
                                WHERE nom = $1',[nom]);
        return id.rows[0].id;
    }       

}

module.exports = new Visiteur();