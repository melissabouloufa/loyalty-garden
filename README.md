# progweb
Membres : 
    Melissa Bouloufa
    Elyas Saidoune

## Name
LOYALTY GARDEN

## Description
Site web de cartes de fidélité pour un magasin de jardinage.

## Initialiser la base de données 
Aller sur psql à l'aide de la commande :
    `psql`
et taper la commande : 
    `\i base_de_donnees`

## Lancer le serveur
Exécuter le fichier mon_site.js (dans son répertoire) : 
    `node mon_site.js`

## Conseil de test
   1- Lancez le serveur et allez sur la page `localhost:8080/`

   2- Cliquez sur "connexion gérante" et tapez le mot de passe "gerante"

   2*- Vous pouvez consulter les produits et les membres

   3- Cliquez sur "ajouter un membre" et complétez le formulaire (attention, il faut respecter le format décrit dans les info-bulles). Mettez en date de naissance celle d'aujourd'hui ! :)

   4- Si vous le voulez, ajoutez un produit en cliquant sur le bouton fait pour ça. Vous pourrez mettre une photo de votre produit

   5- Retournez sur la page principale, recherchez le membre que vous avez créé et ajoutez lui des points (~3000)

   6- Déconnectez-vous du compte de la gérante et retournez à la page d'accueil (route '/')

   7- Connectez-vous au compte du membre que vous avez créé

   8- Admirez l'animation en l'honneur de votre anniversaire et ajoutez les produits que vous souhaitez à votre panier

   9- Consultez votre panier en cliquant sur le bouton en haut à droite, supprimez des articles ou validez le panier

   10- Voilà !


## Utilisation
La page d'accueil vous propose 2 types de connexion : 

    1. Connexion d'une gérante : 
        ¤ Le mot de passe est "gerante".
        ¤ Elle peut consulter les membres, en chercher un en fonction de son nom ou de son prénom (ou les deux) et voir ses informations, lui ajouter ou lui retirer des points, supprimer son compte.
         -> La création d'un membre se fait à l'aide d'un formulaire (cliquer sur le bouton "ajouter un membre")
        ¤ Elle peut consulter les cadeaux, en chercher un en fonction de son nom et voir ses informations et le supprimer.
         -> L'ajout d'un article se fait à l'aide d'un formulaire
          (cliquer sur le bouton "ajouter un article")
        ¤ Elle peut se déconnecter.

    2. Connexion d'une cliente :
        ¤ Utilisez, par exemple : 
            email : melissa@gmail.com
            mot de passe : abcde
        ¤ Elle peut consulter le catalogue des cadeaux en fonction des catégories (les pots, les plantes, les cadeaux recommandés, les cadeaux qui peuvent être achetés (pour vous)...).
         -> Les produits "pour vous" s'affichent en fonction du nombre de points de la cliente en prenant en compte ceux du panier. Si vous avez 1000 points et que votre panier contient un article de 100 points, nous vous proposerons seulement les articles valant au plus 900 points.
        ¤ Elle peut voir ses points de base en permanence (en haut à droite).
        ¤ Elle peut consulter son panier (cliquer sur le bouton en forme de panier en haut à droite).
        ¤ Elle peut supprimer des articles de son panier ou le valider et passer au paiement.

        --- Si c'est votre anniversaire --- 
        ¤ Une animation avec des confettis et un texte vous souhaitant bon anniversaire s'afficheront sur la page, et une nouvelle catégorie de cadeaux (spécial anniversaire) s'affichera.
        ¤ Le pot gratuit appelé "Pot Spécial" ne peut être ajouté qu'une seule fois au panier.