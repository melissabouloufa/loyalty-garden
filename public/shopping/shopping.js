$(document).ready (function () {

    function tourne() {
        $('#text_anniv').animate({left: '-100%'}, 8000, 'linear', function() {
            $(this).css('left', '100%');
            tourne();
        });
    }

    // Animation d'anniversaire
    const jsConfetti = new JSConfetti();
    let anniversaire = function() {
        $.get("http://localhost:8080/anniversaire", {},
            function(data) {
                if(data[0]) {
                    $("#anniversaire_cadeaux").show();

                    $("#recommandations").css("top", "820px");
                    $("#populaires").css("top", "1280px");
                    $("#plantes").css("top", "1740px");
                    $("#bouquets").css("top", "2160px");
                    $("#pots").css("top", "2620px");

                    $("#anniversaire").show();
                    $("#anniversaire").html("<p id='text_anniv'>\
                                            Joyeux anniversaire "+data[1]+"\
                                             ! Nous vous souhaitons\
                                             tous nos meilleurs voeux</p>");
                    tourne();
                    jsConfetti.addConfetti({
                        emojis: ['🎈','🎉','🎊','💐', '🌷', '✨', '💫', '🌸'],
                    }).then(() => jsConfetti.addConfetti())
                }
            }
        );
    };

    anniversaire();

    $("#deconnexion").click(function() {
        $.get("http://localhost:8080/deconnexion", {}, 
            function(data) {
                if(data) window.location.reload();
            });
    });

    $("#panier").click(function() {
        window.location.href = '/panier';
    });

    $("#fond_header").click(function() {
        window.location.reload();
    });

    $("#panier").hover(
        function() {
            $(this).stop().animate({height : "70px", width : "70px"}, 100);
        }, 
        function() {
            $(this).stop().animate({height : "60px", width : "60px"}, 100);
        });

        
    let animationEnCours = false;
    $("form").submit(function(event){
        event.preventDefault();
        if(!animationEnCours) {
            animationEnCours = true;
            let form = $(this);
            let image = $(this).closest(".produit").find("#img");
            let new_image = "<img id='anim-image' src='"+image.attr('src')+"' alt='image'>";
            $("body").append(new_image);
            let new_position = image.offset();
            $("#anim-image").css({
                position: "absolute",
                width: "auto",
                height: image.height(),
                top : new_position.top,
                left: new_position.left,
                opacity:"0.5"
            });
            let position_panier = $("#panier").offset()

            /* animation à l'ajout d'un cadeau au panier */
            $("#anim-image").animate({
                top : position_panier.top,
                left : position_panier.left,
                height :"5px"
            },1000, function(){
                $(this).remove();
                /* Attendre que l'animation précédente soit
                terminée avant d'en démarrer une nouvelle*/
                animationEnCours = false;
                let id = form.find('[name="id"]').val();
                let categorie = form.find('[name="categorie"]').val();
                let pot = form.find('[name="choix-pot"]').val();
                let couleur= form.find('[name="choix-couleur"]').val()
                $.post("http://localhost:8080/loyalty-garden/shopping", {
                    id:id,
                    categorie:categorie,
                    pot:pot,
                    couleur:couleur
                }, 
                    function(data) {
                        if(data) window.location.reload();
                    }
                );
            });
        }
    });
    
});