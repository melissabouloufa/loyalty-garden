$(document).ready (function () {

    $("#amdp").change(function() {
        if($("#amdp").prop("checked")){
            $("#mdp").attr('type', 'text');
        }
        else {
            $("#mdp").attr('type', 'password');
        }
    });

    let champs = $("#email, #mdp");

    champs.keyup(function() {
        let ok = true;
        champs.each(function() {
            if($(this).val().trim()==""){
                ok = false;
                $(this).css("border-color","red");
            }
            else {
                $(this).css("border-color","black");
            }
        });
        $("#send").attr("disabled", !ok);
    });

   $("#send").on ("click", function(event) {
        event.preventDefault();
        let email = $("#email").val();
        let mdp = $("#mdp").val();
        $.post("http://localhost:8080/connexion", { 
            mdp: mdp, 
            email: email
            }, function(data) {
                if(data) {
                    $("p").hide();
                    $("#non_connecte").hide();
                    $("#connecte").show();
                    window.location.href = '/shopping';
                }
                else {
                    $("p").show();
                }
            });
   });

});