$(document).ready (function () {

    $(".supprimer").click(function() {
        let formulaire = $(this).closest(".info").find("form");
        let id = formulaire.find("#id_produit").val();
        let categorie = formulaire.find("#cat_produit").val();
        let send_data;
        if(categorie=='pot'){
            let couleur = formulaire.find("#couleur_produit").val();
            send_data = {
                id:id,
                categorie:categorie,
                couleur:couleur
            }
        }
        else{
            send_data = {
                id:id,
                categorie:categorie
            }
        }
        
        $.post("http://localhost:8080/loyalty-garden/panier/supprimer", send_data, 
            function(data) {
                if(data) window.location.reload();
            });
    });

    $(".valider").click(function() {
        
        $.post("http://localhost:8080/loyalty-garden/panier/", {}, 
            function(data) {
                if(data) {
                    window.location.reload();
                } 
                else{
                    $("#validation-impossible").show();
                }
            });
    });

    $("#retour").click(function() {
        window.location.href = '/shopping';
    });

    $("#fond_header").click(function() {
        window.location.href = '/shopping';
    });
    
});