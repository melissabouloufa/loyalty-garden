$(document).ready (function () {

    $("#retour").click(function(event) {
        window.location.href = '/gerante';
    });

    // forcer le nom et prénom sans caractères spéciaux
    $("#nom, #prenom").keyup(function() {
        let valeur = $(this).val();
        valeur = valeur.replace(/[^A-Za-z-]/g, '');
        if(valeur.length>0){
            valeur = valeur[0].toUpperCase() 
                    + valeur.substring(1).toLowerCase();
        }
        $(this).val(valeur);
        $("#identifiant").val(
            $("#civilite").val()+" "
            +$("#nom").val() + " "
            +$("#prenom").val()
        );
    });

    $("#email").keyup(function() {
        let valeur = $(this).val();
        $(this).val(valeur.toLowerCase());
    });

    $("#mdp").keyup(function() {
        // interdire les espaces dans le mot de passe
        let valeur = $(this).val();
        valeur = valeur.replace(/\s/g, '');
        $(this).val(valeur);
    });

    $("#amdp").change(function() {
        if($("#amdp").prop("checked")){
            $("#mdp, #cmdp").attr('type', 'text');
        }
        else {
            $("#mdp, #cmdp").attr('type', 'password');
        }
    });

    let champs = $("#nom, #prenom, #date, #mdp, #cmdp, #email");

    champs.keyup(function() {
        let ok = true;
        
        // vérifier que les champs sont bien remplis
        champs.each(function() {

            if($(this).val().trim()==""){
                ok = false;
                $(this).css("border-color","red");
            }
            else {
                $(this).css("border-color","black");
            }
        });

        // Verification du format de l'email entrée
        let format_mail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
        if(!format_mail.test($("#email").val())) {
            ok = false;
            $("#email").css("border-color","red");
        }

        if($("#mdp").val().length < 5) {
            ok = false;
            $("#mdp").css("border-color", "red");
        }

        if($("#mdp").val()!=$("#cmdp").val()){
            ok = false;
            $("#cmdp").css("border-color", "red");
        }

        $("#send").attr("disabled", !ok);
    });

   $("#send").on ("click", function(event) {
        event.preventDefault();
        let civilite = $("#civilite").val();
        let f = (civilite == "Mme") ? "e " : " ";
        let nom = $("#nom").val();
        let prenom = $("#prenom").val(); 
        let mdp = $("#mdp").val();
        let email = $("#email").val();
        let naissance = $("#date").val();
        $.post("http://localhost:8080/loyalty-garden/gerante/inscription", { 
            nom: nom,
            prenom: prenom,
            mdp: mdp, 
            email: email,
            naissance: naissance
            }, function(data) {
                if(data) {
                    $("#email_existe").hide();
                    $("#non_inscrit").hide();
                    $("#inscrit").show().append(civilite+" "+nom+" "+prenom+" \
                                         a bien été ajouté"+f+" !<br>");
                }
                else {
                    $("#email_existe").show();
                }
            });
   });

});