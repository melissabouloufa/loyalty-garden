$(document).ready (function () {

    $("#deconnexion").click(function() {
        $("#connecte").css("display","none");
        $("#non_connecte").css("display","block");
        $.get("http://localhost:8080/deconnexion", {}, 
            function(data) {
                if(data) window.location.reload();
            });
    });

    $("#inscription").click(function() {
        window.location.href = '/inscription';
    });

    $("#article").click(function() {
        window.location.href = '/gerante/ajout_article';
    });

    let supprimerMembre = function(id) {
        $.post("http://localhost:8080/loyalty-garden/suppMembre", 
            {id: id}, 
            function(data) {
                if(data) { window.location.reload(); }
            });
    };
    
    let modifier_points = function(nb, id) {
        if(nb!==undefined && nb!==0) {
            $.post("http://localhost:8080/loyalty-garden/modifPoints", 
            {pts : nb,
             id : id},
             function(data) {
                if(data) { window.location.reload(); }
             }
            );
        }
    };

    let convertDate = function(dateSQL) {
        let date = new Date(dateSQL);
        let jour = date.getDate();
        let mois = date.getMonth()+1;
        let annee = date.getFullYear();
        jour = jour < 10 ? '0' + jour : jour;
        mois = mois < 10 ? '0' + mois : mois;
        let res = jour+'/'+mois+'/'+annee;
        return res;
    }

    let only_numbers = function(id) {
        let valeur = $(id).val();
        valeur = valeur.replace(/[^0-9]/g, '');
        $(id).val(valeur);
    }

    let majM = function() {
        let nom_entier = $("#query").val();
        let mots = nom_entier.split(' ');
        let prenom = "";
        let nom = "";
        if (mots.length > 0) {
        prenom = mots[0];
        }
        if (mots.length > 1) {
        nom = mots[1];
        }

        $.get("http://localhost:8080/loyalty-garden/gerante/search", 
            {prenom : prenom, nom : nom},
            function(data) {
                $("#results").html("");
                for (let p of data) {
                    let id = "suppM"+p.id;
                    let supp = "<div><button class='supprimer' id='"
                               +id+"'>Supprimer</button></div>";
                    id = "#"+id;

                    let moins = "moins"+p.id;
                    let plus = "plus"+p.id;
                    let nb = "nb"+p.id;
                    let modifPoints = "<div class='number-input'>\
                                         <button class='minus-btn' id='"+moins+"'>-</button>\
                                         <input id='"+nb+"' type='text' class='quantity'>\
                                         <button class='plus-btn' id='"+plus+"'>+</button>\
                                         </div>";
                    moins = "#"+moins;
                    plus = "#"+plus;
                    nb = "#"+nb;

                    $("#results").append("<div class='col-6 col-sm-5 col-md-4 col-lg-3 produit h-80'>\
                                            <div id='nom'>Nom : "+p.nom+"</div>\
                                            <div id='prix'>Prénom : "+p.prenom+"</div>\
                                            <div id='naissane'>Naissance : "+convertDate(p.datenaiss)+"<br></div>\
                                            "+supp+"Points : "+p.points+modifPoints+"\
                                            <div id='mail'><br>"+p.email+"</div>\
                                          </div>");

                    $(nb).keyup(function(){
                        only_numbers(nb);
                    });
                    
                    $("#results").off("click", moins).on("click", moins,function() {
                        modifier_points(-($(nb).val()),p.id);
                    });
                    $("#results").off("click", plus).on("click", plus,function() {
                        modifier_points(+($(nb).val()),p.id);
                    });
                    
                    $(id).click(function() {
                        supprimerMembre(p.id);
                    });
                }
            }
        );
    };

    $("#query").on('keyup',(majM));

    let supprimerProd = function(id) {
        $.post("http://localhost:8080/loyalty-garden/suppProduit", 
            {id: id}, 
            function(data) {
                if(data) { window.location.reload(); }
            });
    };

    let majP = function() {
        let prod = $("#query2").val();
        if(prod.length>0){
            prod = prod[0].toUpperCase() 
                    + prod.substring(1).toLowerCase();
        }
        if (prod===undefined) {
            prod = "";
        }
        $.post("http://localhost:8080/loyalty-garden/gerante/search2",
            {nom : prod}, 
            function(data) {
                $("#results2").html("");
                for (let p of data) {
                    let id = "supp"+p.id;
                    let supp = "<div>\
                                    <button class='supprimer' id='"+id+"'>Supprimer</button>\
                                </div>";
                    id = "#"+id;
                    $("#results2").append("<div class='col-6 col-sm-5 col-md-4 col-lg-3 produit h-80'>\
                                                <div id='description'>\
                                                    <div id='nom'>"+p.nom+"</div>\
                                                    <div id='prix'>Prix : "+p.prix+"</div>\
                                                    Catégorie : "+p.categorie+"\
                                                    <div>"+supp+"</div>\
                                                </div>\
                                                <div id='image'>\
                                                    <img id='img' src='"+p.photo+"'>\
                                                </div>\
                                            </div>");
                    $(id).click(function() {
                        supprimerProd(p.id);
                    });
                }
            });
    }

    majP();
    majM();

    $("#query2").on('keyup',(majP));

});