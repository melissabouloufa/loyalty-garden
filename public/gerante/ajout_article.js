$(document).ready (function () {

    // forcer un nom sans caractères spéciaux
    $("#nom").keyup(function() {
        let valeur = $(this).val();
        valeur = valeur.replace(/[^A-Za-z- éàçêè'ô]/g, '');
        if(valeur.length>0){
            valeur = valeur[0].toUpperCase() 
                    + valeur.substring(1).toLowerCase();
        }
        $(this).val(valeur);
    });

    $("#retour").click(function(event) {
        window.location.href = '/gerante';
    });

    // forcer un prix de type entier
    $("#prix").keyup(function() {
        let valeur = $(this).val();
        valeur = valeur.replace(/[^0-9]/g, '');
        $(this).val(valeur);
    });

    let champs = $("#nom, #prix");

    champs.keyup(function() {
        let ok = true;
        // vérifier que les champs sont bien remplis
        champs.each(function() {

            if($(this).val().trim()==""){
                ok = false;
                $(this).css("border-color","red");
            }
            else {
                $(this).css("border-color","black");
            }
        });

        if($("#file").files && $("#file").files.length===0) {
            ok = false;
        }

        $("#send").attr("disabled", !ok);
    });

   $("#send").on ("click", function(event) {
        event.preventDefault();
        let categorie = $("#categorie").val();
        let nom = $("#nom").val().trim();
        let prix = $("#prix").val();
        let fic = $("#file")[0].files[0];
        let formData = new FormData(); // pour envoyer le fichier
        formData.append("nom", nom);
        formData.append("categorie", categorie);
        formData.append("prix", prix);
        formData.append("file", fic);
    
        /* Ici, on met contentType et processData à false pour 
           s'assurer que les données sont correctement
           traitées et envoyées */
        $.ajax({
            url: "http://localhost:8080/loyalty-garden/gerante/ajout_article",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data[0]) {
                    $("#nom_existe").hide();
                    $("#non_ajoute").hide();
                    $("#ajoute").show().append("Nouveau produit ajouté avec succès.\
                                                <br><br>\
                                                Nom : "+nom+"<br>\
                                                Prix : "+prix+"<br>\
                                                Catégorie : "+categorie+"<br><br><br>\
                                                <img id='img_prod' src='"+data[1]+"'>");
                    $("#img_prod").css("height", "300px").css("width", "auto");
                } else {
                    $("#nom_existe").show();
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
   });

})