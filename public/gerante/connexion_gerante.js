$(document).ready (function () {

    $("#amdp").change(function() {
        if($("#amdp").prop("checked")){
            $("#mdp").attr('type', 'text');
        }
        else {
            $("#mdp").attr('type', 'password');
        }
    });

    $("#mdp").keyup(function() {
        let ok = true;
        if($(this).val().trim()==""){
            ok = false;
            $(this).css("border-color","red");
        }
        else {
            $(this).css("border-color","black");
        }
        $("#send").attr("disabled", !ok);
    });

    $("#send").on ("click", function(event) {
        console.log("SEND lalala");
        event.preventDefault();
        let mdp = $("#mdp").val();
        $.post("http://localhost:8080/loyalty-garden/gerante", {mdp : mdp}, 
            function(data) {
                console.log("callback post send");
                if(data) {
                    $("p").hide();
                    window.location.href = '/gerante';
                }
                else {
                    $("p").show();
                }
            });
   });

})